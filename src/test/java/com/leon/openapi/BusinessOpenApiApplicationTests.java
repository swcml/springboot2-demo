package com.leon.openapi;

import com.leon.openapi.common.utils.GsonUtils;
import groovy.lang.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessOpenApiApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(BusinessOpenApiApplicationTests.class);

    @Test
    public void contextLoads() {

    }

    public static void main(String[] args) {

        //执行动态脚本字符串
        Binding groovyBinding = new Binding();
        GroovyShell groovyShell = new GroovyShell(groovyBinding);

        String scriptContent = "import com.leon.openapi.common.utils.GsonUtils;" +
                "import com.leon.openapi.common.utils.DateUtils; " +
                "import com.leon.openapi.model.parms.RaceParms \n" +
                "def RaceParms p = new RaceParms();\n" +
                "        p.setDate(DateUtils.getNowDate());\n" +
                "        p.setGameModuleId(35);\n" +
                "        p.setGrounder(1);\n" +
                "        p.setGuessType(0);\n" +
                "        p.setNumtype(1);\n" +
                "        p.setOneraceId(1);\n" +
                "        p.setStart(0);\n" +
                "        p.setLimit(100);\n" +
                "        System.out.println(GsonUtils.GsonString(p));";

        Script script = groovyShell.parse(scriptContent);
        script.run();
        //清除缓存
        groovyShell.getClassLoader().clearCache();


        //执行动态脚本文件
        Object returnVal = null;
        GroovyClassLoader loader = new GroovyClassLoader();
        try
        {
            Class groovyClass = loader.parseClass(new File("F:\\test\\groovy\\HelloWorld.groovy"));
            GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
            returnVal =  groovyObject.invokeMethod("run", "helloworld");

        }
        catch (Exception e)
        {
            log.error("groovy动态执行脚本出错" + e.getMessage(), e);
        }finally {
            loader.clearCache();  //清除缓存，否则无法GC，导致内存溢出问题
        }

        System.out.println(GsonUtils.GsonString(returnVal));
    }

}
