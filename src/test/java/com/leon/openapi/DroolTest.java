package com.leon.openapi;

import com.leon.openapi.common.drools.Address;
import com.leon.openapi.common.drools.AddressCheckResult;
import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DroolTest {

    @Autowired
    private KieBase kieBase;

    @Test
    public void rule(){
        KieSession kieSession = kieBase.newKieSession();

        Address address = new Address();
        address.setPostcode("00000");
        address.setAge(15);

        AddressCheckResult result = new AddressCheckResult();
        kieSession.insert(address);
        kieSession.insert(result);
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("触发了"+ruleFiredCount+"条规则");

        System.out.println(result.isPostCodeResult());
        System.out.println(result.isAgeResult());

        if (result.isPostCodeResult() && result.isAgeResult()){
            System.out.println("规则校验通过");
        }
    }

}
