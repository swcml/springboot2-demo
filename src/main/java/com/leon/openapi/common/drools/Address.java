package com.leon.openapi.common.drools;

import lombok.Data;

@Data
public class Address {

    private String postcode;

    private int age;
}
