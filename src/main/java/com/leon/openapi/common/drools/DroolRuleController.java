package com.leon.openapi.common.drools;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Slf4j
@Controller
public class DroolRuleController {

    @Autowired
    private KieBase kieBase;

    //http://localhost:8080/rule
    @RequestMapping("/rule")
    @ResponseBody
    public boolean rule(){
        //StatefulKnowledgeSession
        KieSession kieSession = kieBase.newKieSession();

        Address address = new Address();
        address.setPostcode("9992223");
        address.setAge(16);

        AddressCheckResult result = new AddressCheckResult();
        kieSession.insert(address);
        kieSession.insert(result);
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("触发了"+ruleFiredCount+"条规则");

        if (result.isPostCodeResult() && result.isAgeResult()){
            System.out.println("规则校验通过");
        }

        return result.isPostCodeResult() && result.isAgeResult();
    }
}
