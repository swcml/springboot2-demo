package com.leon.openapi.common.drools;

import lombok.Data;

@Data
public class AddressCheckResult {

    private boolean postCodeResult = false; // true:通过校验；false：未通过校验
    private boolean ageResult = false; // true:通过校验；false：未通过校验
}
