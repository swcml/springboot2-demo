package com.leon.openapi.common.utils;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * logback 日志 ip转换辅助
 * @package: com.guess.openapi.utils
 * @author: leon
 * @date: 2017/11/8 17:05
 * @ModificarionHistory who     when   what
 * --------------|------------------|--------------
 */
public class IpConvert extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {

        return CommonUtils.getLocalHostLANAddress();
    }
}
