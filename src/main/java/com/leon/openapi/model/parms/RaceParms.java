package com.leon.openapi.model.parms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="查询对象",description="查询对象RaceParms")
public class RaceParms implements Serializable{

    @ApiModelProperty(value="模块",name="gameModuleId",example="")
    private Integer gameModuleId;

    @ApiModelProperty(value="类型，0普通、1特殊....",name="guessType",example="")
    private Integer guessType;

    private Integer oneraceId;
    private Date date;
    private Integer start;
    private Integer limit;

    @ApiModelProperty(value="0今日 1赛前 2已结束",name="numtype",example="")
    private Integer numtype;

    @ApiModelProperty(value="0  1",name="grounder",example="")
    private Integer grounder;

}
