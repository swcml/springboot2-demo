package com.leon.openapi.model.obj;

import com.leon.openapi.model.Race;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RaceObj extends Race {

	private String pvpTeam1Name;
	private String pvpTeam1IconUrl;
	private String pvpTeam2Name;
	private String pvpTeam2IconUrl;

}
