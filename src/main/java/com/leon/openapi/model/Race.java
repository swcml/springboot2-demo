package com.leon.openapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Race implements Serializable {
    private Integer id;

    private Integer gameModuleId;

    private String eventName;

    private Integer guessType;

    private Integer pvpTeam1Id;

    private Integer pvpTeam2Id;

    private Integer turnBased;

    private Date startTime;

    private Integer grounder;

    private String result;

    private String liveUrl;

    private String explains;

    private Integer flag;

    private String flagExplain;

    private Date createTime;

    private Integer online;
}