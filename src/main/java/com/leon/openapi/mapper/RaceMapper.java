package com.leon.openapi.mapper;

import com.leon.openapi.model.Race;
import com.leon.openapi.model.obj.RaceObj;
import com.leon.openapi.model.parms.RaceParms;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface RaceMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Race record);

    int insertSelective(Race record);

    Race selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Race record);

    int updateByPrimaryKey(Race record);

    Integer getRaceListCount(RaceParms raceParms);

    Map<String, Integer> getAllTypeNumMap(RaceParms raceParms);

    List<RaceObj> getRaceList(RaceParms raceParms);
}