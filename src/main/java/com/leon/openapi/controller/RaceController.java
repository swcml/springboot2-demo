package com.leon.openapi.controller;

import com.github.pagehelper.PageInfo;
import com.leon.openapi.common.base.AjaxResult;
import com.leon.openapi.common.utils.GsonUtils;
import com.leon.openapi.model.obj.RaceObj;
import com.leon.openapi.model.parms.RaceParms;
import com.leon.openapi.service.IRaceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Api("比赛接口")
@RestController
@RequestMapping("/race")
public class RaceController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(RaceController.class);

    @Autowired
    private IRaceService raceService;

    /**
     * 首页比赛列表
     * {"gameModuleId":1,"guessType":1,"oneraceId":1,"date":"May 2, 2019 4:41:47 PM","start":0,"limit":100,"numtype":1,"grounder":1}
     */
    @GetMapping(value = "list")
    @ApiOperation(value = "获取比赛列表", notes = "分页查询比赛列表")
    @ApiImplicitParam(name = "jsonData", value = "RaceParms查询JSON字符串", required = true, dataType = "string", paramType = "query")
    public AjaxResult list(@RequestParam(name = "jsonData",required = true)  String jsonData) {

        RaceParms raceParms = GsonUtils.GsonToBean(jsonData,RaceParms.class);
        //默认一次性获取所有类型的个数
        Map<String,Integer> allTypeNumMap = raceService.getAllTypeNumMap(raceParms);

        PageInfo<RaceObj> page = raceService.getRaceList(raceParms);

        return success(GsonUtils.GsonString(page));
    }


}
