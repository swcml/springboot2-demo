package com.leon.openapi.service;


import com.github.pagehelper.PageInfo;
import com.leon.openapi.model.obj.RaceObj;
import com.leon.openapi.model.parms.RaceParms;

import java.util.Map;

public interface IRaceService {

    Integer getRaceListCount(RaceParms raceParms);

    Map<String,Integer> getAllTypeNumMap(RaceParms raceParms);

    PageInfo<RaceObj> getRaceList(RaceParms raceParms);
}
