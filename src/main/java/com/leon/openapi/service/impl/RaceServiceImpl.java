package com.leon.openapi.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.leon.openapi.mapper.RaceMapper;
import com.leon.openapi.model.obj.RaceObj;
import com.leon.openapi.model.parms.RaceParms;
import com.leon.openapi.service.IRaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RaceServiceImpl implements IRaceService {

    @Autowired
    private RaceMapper raceMapper;

    @Override
    public Integer getRaceListCount(RaceParms raceParms) {
        return raceMapper.getRaceListCount(raceParms);
    }


    @Override
    @Cacheable(cacheNames = "redis-cache-5m",key = "'AllTypeNum:' + #p0.gameModuleId+':'+ #p0.guessType +':' + #p0.date")
    public Map<String, Integer> getAllTypeNumMap(RaceParms raceParms) {
//        System.out.println("执行SQL");
        return raceMapper.getAllTypeNumMap(raceParms);
    }

    @Override
    public PageInfo<RaceObj> getRaceList(RaceParms raceParms) {

        return PageHelper.startPage(1, 10,"game_module_id desc,start_time").doSelectPageInfo(() -> raceMapper.getRaceList(raceParms));
    }
}
